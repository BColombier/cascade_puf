# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: CASCADE
# File: get_parities_from_indices.py
# Date : 2016-10-12

def get_parities_from_indices(indices, response_on_board):
    
    """Get the parities of the on-board response blocks.

    Hardware-specific implementation.
    >>> get_parities_from_indices([[1, 5], [7, 2], [3, 6], [4, 0]])
    [0, 1, 1, 0]
    """

    parities = []
    for i in indices:
        parity = 0
        for index in i:
            parity^=response_on_board[index]
        parities.append(parity)
    return parities

if __name__ == "__main__":
    print get_parities_from_indices([[0, 1, 2, 3], [4, 5, 6, 7]], [0, 1, 0, 1, 1, 1, 0, 1])
