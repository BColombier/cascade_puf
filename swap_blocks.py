# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: CASCADE
# File: swap_blocks.py
# Date : 2016-10-12


def swap_blocks(even_parity_blocks, odd_parity_blocks, index, block_size_increased = False, block_size = 4):
    
    """Swap blocks between two lists

    Moves the blocks containing a specific index between two lists
    >>> swap_blocks([[1, 2], [3, 4]], [[5, 6], [7, 8]], 4)
    [[1, 2]], [[3, 4], [5, 6], [7, 8]]
    """

    if block_size_increased:
        block_from_even = [x for x
                           in even_parity_blocks
                           if index in x
                           and len(x) < block_size]
    else:
        block_from_even = [x for x in even_parity_blocks if index in x]
    block_from_odd = [x for x in odd_parity_blocks if index in x]
    while block_from_even:
        block = block_from_even.pop()
        odd_parity_blocks.append(block)
        even_parity_blocks.remove(block)
    while block_from_odd:
        block = block_from_odd.pop()
        odd_parity_blocks.remove(block)
        even_parity_blocks.append(block)
                
