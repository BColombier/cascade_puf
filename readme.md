# CASCADE
Implementation of the CASCADE key reconciliation protocol in Python.
#### Authors:
Brice Colombier, Lilian Bossuet, David H�ly
Laboratoire Hubert Curien
18 rue Pr. Beno�t Lauras, 42000 Saint-Etienne, FRANCE

Source code associated to [this research article](http://eprint.iacr.org/2016/939.pdf).
