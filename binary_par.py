# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: CASCADE
# File: binary_par.py
# Date : 2016-10-12

from parity import parity
from get_parities_from_indices import get_parities_from_indices


def binary_par(reference_response, blocks_to_correct, response_on_board):

    """Implementation of the BINARY algorithm found in the CASCADE protocol.

    From a reference response and a list of blocks to correct, it obtains the
    parities of the blocks on board. According to these parities, it narrows
    down to either the first or the second half of each block.

    """
    
    block_size = int(len(blocks_to_correct[0]))
    if block_size < 2:
        raise ValueError("Illegal block size: too short", blocks_to_correct)
    # Compute the parity of the first half of the response blocks
    blocks_to_correct_first_half = [x[:int(len(x)/2)] for x in blocks_to_correct]
    blocks_return = []
    ref_rep = []
    parities = get_parities_from_indices(blocks_to_correct_first_half, response_on_board)
    if len(blocks_to_correct[0]) > 2:
        for i, j in enumerate(parities):
            if parity(reference_response[i][:block_size/2], j):
                # Error is in the first half of the block
                blocks_return.append(blocks_to_correct[i][:block_size/2])
                ref_rep.append(reference_response[i][:block_size/2])
            else:
                # Error is in the second half of the block
                blocks_return.append(blocks_to_correct[i][block_size/2:])
                ref_rep.append(reference_response[i][block_size/2:])
        return ref_rep, blocks_return
    elif len(blocks_to_correct[0]) == 2:
        for i, j in enumerate(parities):
            if reference_response[i][0] == j:
                blocks_return.append(blocks_to_correct[i][1])
            else:
                blocks_return.append(blocks_to_correct[i][0])
        return ref_rep, blocks_return

if __name__ == "__main__":
    list_in = [[186, 254, 7, 55, 186, 254, 7, 55],
               [186, 254, 7, 55, 186, 254, 7, 55],
               [186, 254, 7, 55, 186, 254, 7, 55],
               [186, 254, 7, 55, 186, 254, 7, 55]]
    res = binary_par(list_in, "bus_script_in1.txt", "bus_script_out1.txt")
    print(res)
    print([x[0] for x in res])
    
