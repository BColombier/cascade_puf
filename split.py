# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: CASCADE
# File: split.py
# Date : 2016-10-12

def split(m, size):
    
    """Split a list into blocks of equal size

    >>> split([0, 1, 0, 1], 2)
    [[0, 1], [0, 1]]
    >>> split([0, 1, 0, 1, 0, 1, 0, 1], 4)
    [[0, 1, 0, 1], [0, 1, 0, 1]]
    
    """
    
    m = [m[i:i + size] for i in xrange(0, len(m), size)]
    return m
    # m_ret = m
    # for i in range(int(len(m)/size)):
        # m_ret.append([m_ret.pop(0) for w in range(int(size))])
    # return m_ret

if __name__ == "__main__":
    message_Alice = [0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0]
    message_Bob   = [0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0]
    size = 8
    message_Alice = split(message_Alice, size)
    print(message_Alice)
    print(message_Bob)
