# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: CASCADE
# File: parity.py
# Date : 2016-10-12


def parity(vect_in_A, par_B):
    
    """Check if the vector parity is equal to a given parity

    >>> parity([0, 1, 0, 1], 0)
    0
    >>> parity([1, 1, 0, 1], 1)
    0
    >>> parity([1, 1, 1, 1], 1)
    1
    >>> parity([0, 1, 0, 0], 0)
    1

    """

    par_A = 0
    for i in vect_in_A:
        par_A ^= i
    return int(par_A) ^ int(par_B)

if __name__ == "__main__":
    print(parity([0, 0, 0, 0], 1))
