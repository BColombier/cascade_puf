# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: CASCADE
# File: flatten.py
# Date : 2016-10-12

from itertools import chain


def flatten(liste):

    """Turns a list of lists into a list.

    >>> flatten([[1, 2], [3, 4]])
    [1, 2, 3, 4]
    """

    liste = list(chain.from_iterable(liste))
    return liste

if __name__ == "__main__":
    print flatten([[0, 1, 2, 3], [4, 5, 6, 7]])
